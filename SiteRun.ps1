Set-Item WSMan:\localhost\Client\TrustedHosts -Value '52.157.85.128' -Force
$username = 'adminuser'
$pass = ConvertTo-SecureString -string 'P@$$w0rd1234!' -AsPlainText -Force
$cred = New-Object -typename System.Management.Automation.PSCredential -argumentlist $username, $pass
$session = New-PSSession -ConnectionUri 'http://52.157.85.128:5985' -Credential $cred -SessionOption (New-PSSessionOption -SkipCACheck -SkipCNCheck -SkipRevocationCheck)

Enter-PSSession -ConnectionUri 'http://52.157.85.128:5985' -Credential $cred
Invoke-Command -Session $session -ScriptBlock {
    Remove-Item -LiteralPath "c:\Work" -Force -Recurse
    new-item c:\Work -ItemType Directory
}
Copy-Item -Path C:\Terraform\Terraform_Learning\index.html -Destination C:\Work -ToSession $session

Invoke-Command -Session $session -ScriptBlock {
    Stop-IISSite -Name "Default Web Site"

    Stop-IISSite -Name "MyWebsite"
    Remove-IISSite -Name 'MyWebsite'

    New-IISSite -Name 'MyWebsite' -PhysicalPath 'C:\Work' -BindingInformation "*:80:"
}